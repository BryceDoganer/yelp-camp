var Campground = require("../models/campground");
var Comment = require("../models/comment");

var middlewareObj = {};

middlewareObj.isLoggedIn = function(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  // Send flash message: First variable is used for styling, the second one is the message. 
  req.flash("error", "You need to be logged in to do that.");
  res.redirect("/login");
}

middlewareObj.checkCampgroundOwnership = function(req, res, next) {
  if (req.isAuthenticated()) {
    Campground.findById(req.params.id, function(err, foundCampground) {
      // You need to check if the campground is null because a valid length id could return null and break the app
      if (err || !foundCampground) {
        req.flash("error", "Campground not found.")
        res.redirect("back");
      }
      else {
        // `author.id` is an object and `req.user._id` is a string. Use mongoose's
        //  .equals() to check if they are equal.
        if (foundCampground.author.id.equals(req.user._id)) {
          next();
        }
        else {
          req.flash("error", "You don't have permission to do that.")
          res.redirect("back");
        }
      }
    })
  }
  else {
    req.flash("error", "You need to be logged in to do that.")
    res.redirect("back") // Go back to the previous page. 
  }
}

middlewareObj.checkCommentOwnership = function(req, res, next) {
  if (req.isAuthenticated()) {
    Comment.findById(req.params.comment_id, function(err, foundComment) {
      if (err || !foundComment) {
        req.flash("error", "Comment not found.")
        res.redirect("back");
      }
      else {
        // `author.id` is an object and `req.user._id` is a string. Use mongoose's
        //    .equals() to check if they are equal.
        if (foundComment.author.id.equals(req.user._id)) {
          next();
        }
        else {
          req.flash("error", "You don't have permission to do that.")
          res.redirect("back");
        }
      }
    })
  }
  else {
    req.flash("error", "Youn need to be logged in to do that.")
    res.redirect("back") // Go back to the previous page. 
  }
}



module.exports = middlewareObj;
