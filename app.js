var express = require("express"), //.js isnt't required
  app = express(),
  bodyParser = require("body-parser"),
  mongoose = require("mongoose"),
  passport = require("passport"),
  flash = require("connect-flash"),
  LocalStragey = require("passport-local"),
  methodOverride = require("method-override"),
  Campground = require("./models/campground"), // Include the Campground mongoose model
  Comment = require("./models/comment"), // Include the Comment mongoose model
  User = require("./models/user"),
  seedDB = require("./seeds");

  require('dotenv').config()

var commentRoutes = require("./routes/comments"),
  campgroundRoutes = require("./routes/campgrounds"),
  indexRoutes = require("./routes/index");

// Setup `body-parser` to read POST variables 
app.use(bodyParser.urlencoded({ extended: true }));
// Connect to the mongo data base stored in the environment var DATABASEURL
//  c9:     mongodb://localhost/yelp_camp_v12
//  heroku: mongodb://<user>:<password>@ds127490.mlab.com:27490/yelpcamp
var db_url = process.env.DATABASEURL || "mongodb://localhost/yelp_camp_v12";
mongoose.connect(db_url);
// seedDB(); // Clear the database and seed it 
// Serve all the files in the 'public directory'
app.use(express.static(__dirname + "/public")); //__dirname is the current directory
// Allows us to leave off `.ejs` on render file names 
app.set("view engine", "ejs");
// Have to use methodOverride to send PUT/DELETE requests from an HTML form 
app.use(methodOverride("_method"));
// Setup connect-flash
app.use(flash());

// PASSPORT CONFIGURATION
app.use(require("express-session")({
  secret: "All day long.",
  resave: false,
  saveUninitialized: false
}))
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStragey(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// We can define our own middleware. Every function attatched to app.use() will be called
// with every route
app.use(function(req, res, next) {
  // Store the current user in a local variable available to all view files  
  res.locals.currentUser = req.user;
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
});


// The first variable is concatenated to the route files.  
app.use("/", indexRoutes);
app.use("/campgrounds", campgroundRoutes);
app.use("/campgrounds/:id/comments", commentRoutes);


app.listen(process.env.PORT || 8080, process.env.IP || '127.0.0.1', function() {
  console.log('YelpCamp server has started!')
});
