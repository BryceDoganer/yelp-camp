var mongoose = require("mongoose");

// Setup Campgrounds Schema 
var campgroundSchema = new mongoose.Schema({
  name: String,
  price: String,
  img: String,
  desc: String,
  author: {
   id: {
     type: mongoose.Schema.Types.ObjectId,
     ref: "User"
   },
   username: String
  },
  comments: [
    {
      // Type of ObjectId
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment" // name of the model linked to the ObjectId
    }  
  ]
});
module.exports = mongoose.model("Campground", campgroundSchema);