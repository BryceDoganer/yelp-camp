var express = require("express");
var router = express.Router();
var Campground = require("../models/campground");
// By naming the middleware file `index.js` it will automatically add it by linking the directory
var middleware = require("../middleware");

// INDEX Campground Route - show all campgrounds 
router.get("/", function(req, res) {
  // Get all campgrounds from DB
  Campground.find({}, function(err, foundCampgrounds) {
    if (err) {
      req.flash("error", "Something went wrong.");
      res.redirect("back");
    }
    else {
      // Set `campgrounds` var on `campgrounds.ejs` to this `campgrounds` array from DB.
      // passport stores all of the information of the logged in user via `req.user` (if any)
      res.render("campgrounds/index", { campgrounds: foundCampgrounds });
    }

  });
});

// NEW Campground Route - show form to create a new campground 
router.get("/new", middleware.isLoggedIn, function(req, res) {
  res.render("campgrounds/new");
});

// **Order is important here**
// SHOW Campground Route - shows more info about one campground 
router.get("/:id", function(req, res) {
  // Find the campground with provided ID - populate the comment array with data
  Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground) {
    if (err || !foundCampground) {
      req.flash("error", "Campground not found.");
      res.redirect("back");
    }
    else {
      res.render("campgrounds/show", { campground: foundCampground });
    }
  });
});

// EDIT Campground Route
router.get("/:id/edit", middleware.checkCampgroundOwnership, function(req, res) {
  Campground.findById(req.params.id, function(err, foundCampground) {
    if (err) {
      req.flash("error", "Campground not found.");
      res.redirect("/campgrounds");
    }
    else {
      res.render("campgrounds/edit", { campground: foundCampground });
    }
  });
});

// UPDATE Campground Route
router.put("/:id", middleware.checkCampgroundOwnership, function(req, res) {
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground) {
    if (err) {
      req.flash("error", "Campground not found.")
      res.redirect("/campgrounds");
    }
    else {
      res.redirect("/campgrounds/" + req.params.id);
    }
  })
});

// DESTORY Campground Route 
router.delete("/:id", middleware.checkCampgroundOwnership, function(req, res) {
  Campground.findByIdAndRemove(req.params.id, function(err, removedCampground) {
    if (err) {
      req.flash("error", "Campground not found.")
      res.redirect("/campgrounds");
    }
    else {
      res.redirect("/campgrounds");
    }
  })
})

// CREATE Campground Route - a new campground to DB
// Same url as `app.get` function above, but it's a post route instead 
// *Need `body-parser` here to read post variables 
router.post("/", middleware.isLoggedIn, function(req, res) {
  // Get campground object from form and add to campgrounds array (Need body-parser for this)
  var author = {
    id: req.user._id,
    username: req.user.username
  }
  var newCampground = req.body.campground;
  newCampground.author = author;
  // Create a new campground and save to DB
  Campground.create(newCampground, function(err, newlyCreated) {
    if (err) {
      console.log(err); // TODO need better error handling 
    }
    else {
      console.log(newCampground);
      // Redirect back to campgrounds page if successful (default is to redirect as a `GET` route)
      res.redirect("/campgrounds");
    }
  });
});

module.exports = router;
