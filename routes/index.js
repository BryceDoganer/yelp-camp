var express = require("express");
var router = express.Router();
var Campground = require("../models/campground");
var User = require("../models/user");
var passport = require("passport");

router.get("/", function(req, res) {
  res.render("landing");
});

// REGISTER routes
// SHOW register form
router.get("/register", function(req, res) {
  res.render("register");
});

// Handle sign up logic
router.post("/register", function(req, res) {
  // Create a new user with ONLY the username
  var newUser = new User({ username: req.body.username });
  // Register the user and the password second so passport can hash and store the password
  User.register(newUser, req.body.password, function(err, user) {
    if (err) {
      // `err` is coming from passport and we are using it to display to the user
      req.flash("error", err.message + ".")
      console.log(err);
      return res.render("register");
    }
    passport.authenticate("local")(req, res, function() {
      req.flash("success", "Welcome to YelpCamp " + user.username + "!");
      res.redirect("/campgrounds");
    });
  });
});

// LOGIN Routes
// Show login form
router.get("/login", function(req, res) {
  // Render login page and any flash messages that fall under the "error" category
  res.render("login");
})
// Handle login logic 
// app.post("/login", middleware, callback)
router.post("/login", passport.authenticate("local", {
  successRedirect: "/campgrounds",
  failureRedirect: "/login"
}), function(req, res) {

});

// LOGOUT Route
router.get("/logout", function(req, res) {
  req.logout();
  req.flash("success", "Logged You Out!");
  res.redirect("/campgrounds");
});

module.exports = router;
