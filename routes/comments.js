var express = require("express");
// Need to use mergeParams so that we can pull the campground id from app.js router
var router = express.Router({ mergeParams: true });
var Campground = require("../models/campground");
var Comment = require("../models/comment");
// By naming the middleware file `index.js` it will automatically add it by linking the directory
var middleware = require("../middleware");


// NEW Comment Route
router.get("/new", middleware.isLoggedIn, function(req, res) {
  // Find campground by id
  Campground.findById(req.params.id, function(err, campground) {
    if (err || !campground) {
      req.flash("error", "Something went wrong.")
      res.redirect("/campgrounds");
    }
    else {
      res.render("comments/new", { campground: campground })
    }
  });
});

// CREATE Comment Route 
router.post("/", middleware.isLoggedIn, function(req, res) {
  var newComment = req.body.comment; // Take comment object sent from `new.ejs` form 
  Campground.findById(req.params.id, function(err, thisCampground) {
    if (err) {
      req.flash("error", "Something went wrong.")
      res.redirect("/campgrounds");
    }
    else {
      Comment.create(newComment, function(err, newDBComment) {
        if (err) {
          console.log(err);
        }
        else {
          newDBComment.author.id = req.user._id;
          newDBComment.author.username = req.user.username;
          newDBComment.save();
          thisCampground.comments.push(newDBComment);
          thisCampground.save();
          req.flash("success", "Successfully added comment.")
          res.redirect("/campgrounds/" + req.params.id);
        }
      });
    }
  });
});

// EDIT Comment Route
router.get("/:comment_id/edit", middleware.checkCommentOwnership, function(req, res) {
  // Check to see if the campground exists because a user can input a bad id and break the app otherwise.
  Campground.findById(req.params.id, function(err, foundCampground) {
    if (err || !foundCampground) {
      req.flash("error", "Campground not found.")
      return res.redirect("back");
    }
    Comment.findById(req.params.comment_id, function(err, foundComment) {
      if (err || !foundComment) {
        req.flash("error", "Something went wrong.")
        res.redirect("back");
      }
      else {
        res.render("comments/edit", {
          campground_id: req.params.id,
          comment: foundComment
        });
      }
    });
  });
});

// UPDATE Comment Route
router.put("/:comment_id", middleware.checkCommentOwnership, function(req, res) {
  Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, foundComment) {
    if (err || !foundComment) {
      req.flash("error", "Comment not found.")
      res.redirect("back");
    }
    else {
      req.flash("success", "Comment successfully updated.")
      res.redirect("/campgrounds/" + req.params.id);
    }
  })
})

// DESTORY Comment Route
router.delete("/:comment_id", middleware.checkCommentOwnership, function(req, res) {
  Comment.findByIdAndRemove(req.params.comment_id, function(err, deletedComment) {
    if (err) {
      req.flash("error", "Something went wrong.")
      res.redirect("back");
    }
    else {
      req.flash("success", "Comment removed.")
      res.redirect("/campgrounds/" + req.params.id);
    }
  })
})

module.exports = router;
